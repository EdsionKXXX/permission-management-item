package cn.kwq.Security.service;


import cn.kwq.Security.domain.ResponseResult;
import cn.kwq.Security.domain.User;

public interface LoginServcie {
    ResponseResult login(User user);

    ResponseResult logout();

}
