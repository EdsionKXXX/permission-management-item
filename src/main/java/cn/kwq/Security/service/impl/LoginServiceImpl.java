package cn.kwq.Security.service.impl;


import cn.kwq.Security.domain.LoginUser;
import cn.kwq.Security.domain.ResponseResult;
import cn.kwq.Security.domain.User;
import cn.kwq.Security.service.LoginServcie;
import cn.kwq.Security.utils.JwtUtil;
import cn.kwq.Security.utils.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 登录验证
 * 调用AuthenticationManager的authenticate方法来进行用户认证，链条第三道
 * bean在config注入
 */
@Service
public class LoginServiceImpl implements LoginServcie {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;

    @Override
    public ResponseResult login(User user) {
        //用户名和密码封装成security用的对象
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUserName(),user.getPassword());
        //AuthenticationManager authenticate进行用户认证
        //链条第三道工序认证，将前端给的username和密码封装好的对象和
        //自己重写的UserDetailsService所查出来的用户信息比较（已经在authenticationManager里了）
        //框架自动帮我们调用PasswordEncoder进行比较，其中比较方法为我们注入的加密方法
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        //如果认证没通过，给出对应的提示
        if(Objects.isNull(authenticate)){
            throw new RuntimeException("登录失败");
        }
        //在Authentication对象中取出验证完的对象的信息
        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();
        //如果认证通过了，使用userid生成一个jwt jwt存入ResponseResult返回
        String userid = loginUser.getUser().getId().toString();
        String jwt = JwtUtil.createJWT(userid);
        Map<String,String> map = new HashMap<>();
        map.put("token",jwt);
        //把完整的用户信息存入redis  userid作为key
        redisCache.setCacheObject("login:"+userid,loginUser);
        return new ResponseResult(200,"登录成功",map);
    }

    @Override
    public ResponseResult logout() {
        //获取SecurityContextHolder中的用户id
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        Long userid = loginUser.getUser().getId();
        //删除redis中的值
        redisCache.deleteObject("login:"+userid);
        return new ResponseResult(200,"注销成功");
    }
}
